#!/usr/bin/env bash
read -p "Enter homeserver url (non-delegated; with https): " hs
read -p "Enter shared secret to be used for registering the accounts: " sharedsecret
read -p "Enter password to be used for the accounts: " pass
read -p "Room ID (must be in auto_join however): " roomid
while IFS= read -r line; do
	nonce="$(curl ${hs}/_synapse/admin/v1/register | jq -r '.nonce')"
	sym="$(echo $line | awk '{print $1}')"
	elem="$(echo $line | awk '{print $2}')"
	username="${elem,,}"
	display_name="${elem} (${sym})"
	mac_digest="$(printf '%s\0%s\0%s\0%s' "$nonce" "$username" "$pass" "notadmin" | openssl sha1 -hmac "$sharedsecret" | awk '{print $2}')"

	json='{"nonce":"'"${nonce}"'","username":"'"${username}"'","displayname":"'"${display_name}"'","password":"'"${pass}"'","admin": false,"mac":"'"${mac_digest}"'"}'
  	access_token=$(curl -s -X POST -H 'Content-Type: application/json' -d "${json}"	"$hs"'/_synapse/admin/v1/register' | jq -r '.access_token')
	msg="I am $elem. My symbol is $sym and I was discovered in $(echo $line | awk '{print $NF}')."
	
  	curl -s \
    	-X PUT \
    	-H 'Content-Type: application/json' \
    	-H 'Authorization: Bearer '"$access_token" \
    	-d '{"msgtype":"m.text", "body":"'"$msg"'"}' \
    	"$hs"'/_matrix/client/r0/rooms/'"$roomid"'/send/m.room.message/'
done < "./ptable.txt"
